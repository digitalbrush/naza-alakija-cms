/*!
    * Start Bootstrap - Grayscale v6.0.2 (https://startbootstrap.com/themes/grayscale)
    * Copyright 2013-2020 Start Bootstrap
    * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-grayscale/blob/master/LICENSE)
    */

(function ($) {
    "use strict"; // Start of use strict

    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (
            location.pathname.replace(/^\//, "") ==
                this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length
                ? target
                : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html, body").animate(
                    {
                        scrollTop: target.offset().top - 70,
                    },
                    1000,
                    "easeInOutExpo"
                );
                return false;
            }
        }
    });

    jQuery(document).ready(function($){
        orientationChange();
    });
    
    function orientationChange() {
        if(window.addEventListener) {
            window.addEventListener("orientationchange", function() {
                location.reload();
            });
        }
    }

    $("#readsAccordion").on("hide.bs.collapse show.bs.collapse", function(e) {
        $(e.target).prev().find("i:last-child").toggleClass("fa-minus fa-plus");
      });
      

    // Closes responsive menu when a scroll trigger link is clicked
    // $(".nav-link").click(function () {
    //     $(".navbar-collapse").hide();
    // });


    $(document).ready(function() {
        if ($('body').hasClass('modal-open')) {
            $.fn.fullpage.setAllowScrolling(false);
        }
    });

    $(document).ready(function() {
        if ($('body').hasClass('home')) {
            $('footer').hide();
        }
    });

    $('.naza-tilt').tilt({
        scale: 1.1
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $("body").scrollspy({
        target: "#mainNav",
        offset: 100,
    });

    $(document).ready(function() {
        if(window.matchMedia("(max-height: 420px)").matches){
            $('section').addClass('fp-auto-height-responsive');
            $('header').addClass('fp-auto-height-responsive');
            $('.about-section').find('div.container').removeClass('container').addClass('container-fluid');
            $('.humanitarian-section').find('div.container').removeClass('container').addClass('container-fluid');
        }
        else {
            $('section').removeClass('fp-auto-height-responsive');
            $('header').removeClass('fp-auto-height-responsive');
            $('.about-section').find('div.container-fluid').removeClass('container-fluid').addClass('container');
            $('.humanitarian-section').find('div.container-fluid').removeClass('container-fluid').addClass('container');
        }
    });

    $(document).ready(function() {
        if(window.matchMedia("(max-width: 480px)").matches){
            $('#contactForm').appendTo('#contactModalForm');
        }
        else {
            $('#contactModalForm').appendTo('#contactForm');
        }
    });

    // variables
    var $nav = $('#mainNav');

    $('#fullpage').fullpage({
        navigation: true,
        controlArrows: false,
        anchors: ['welcome', 'about', 'humanitarian', 'environmentalist', 'philanthropist', 'contact'],
        css3: true,
        scrollingSpeed: 700,
        autoScrolling: true,
        navigation: false,
        animateAnchor: true,
        scrollBar: false,
        normalScrollElements: '.modal',
        fixedElements: 'nav',
        fitToSection: true,
        responsiveHeight: 420,
        lazyLoading: true,
        licenseKey: 'CD64714F-DAC24521-B811496E-7F2AF398',

        afterLoad: function(origin, destination, direction){
            var loadingSection = this;

            if(destination.anchor == 'welcome'){
                $nav.find('a').css(
                    {
                        'color': '#2B1919',
                    }
                );
                $nav.find('.line').css('background', '#2B1919');
                $('.hero-naza').addClass('animate__pulse');
            }
            if(destination.anchor == 'about'){
                $nav.find('a').css(
                    {
                        'color': '#342323',
                    }
                );
                $nav.find('.line').css('background', '#342323');
                $('.about-block').addClass('animate__flipInY');
            }
            if(destination.anchor == 'humanitarian'){
                $nav.find('a').css(
                    {
                        'color': '#fff',
                    }
                );
                $nav.find('.line').css('background', '#fff');
                $('.hblock-text h5').addClass('animate__lightSpeedInRight animate__swing');
            }
            if(destination.anchor == 'environmentalist'){
                $nav.find('a').css(
                    {
                        'color': '#fff',
                    }
                );
                $nav.find('.line').css('background', '#fff');
                $('.env-heading-text').addClass('animate__bounceInDown');
            }
            if(destination.anchor == 'philanthropist'){
                $nav.find('a').css(
                    {
                        'color': '#885F4F',
                    }
                );
                $nav.find('.line').css('background', '#885F4F');
                $('.phil-heading-text').addClass('animate__bounceInDown');
                $('.sage-logo').addClass('animate__zoomIn');
            }
            if(destination.anchor == 'contact'){
                $nav.find('a').css(
                    {
                        'color': '#FEF8F3',
                    }
                );
                $nav.find('.line').css('background', '#FEF8F3');
                $('.contact-heading-text').addClass('animate__bounceInDown');
                $('.naza-contact').addClass('animate__jackInTheBox');
            }
        }
    });

    $('.nav-menu-icon').on('click', function () {
        $(this).toggleClass('active animate__fadeInLeft');
        if(window.matchMedia("(max-width: 767px)").matches){
            $('.nav-menu-icon').parent().parent().siblings('div.logo').toggleClass('animate__fadeOutRight');
            $('.nav-menu-icon').parent().parent().siblings('ul').toggleClass('animate__fadeOutRight');
        }
    });
    $(window).on("load",function(){
        $(".loader").fadeOut("slow");
        $("#overlayer").fadeOut("slow");
    });


    // collapse Navbar when menu item is selected
    $('.menu-item').on('click', function (e) {
        e.preventDefault();
        $('.nav-menu-icon').toggleClass('active animate__fadeInLeft');
        if(window.matchMedia("(max-width: 767px)").matches){
            $('.nav-menu-icon').parent().parent().siblings('div.logo').toggleClass('animate__fadeOutRight');
            $('.nav-menu-icon').parent().parent().siblings('ul').toggleClass('animate__fadeOutRight');
        }
    });

    $('.menu-item').on('click', function () {
        $('.navbar-collapse').removeClass('show');
    });

    // Collapse Navbar
    var navbarCollapse = function () {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
        } else {
            $("#mainNav").removeClass("navbar-shrink");
        }
    };

    $(".wp-block-image img").addClass("img-fluid");
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);
})(jQuery); // End of use strict
