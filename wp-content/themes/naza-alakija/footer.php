<?php
/**
 * The template for displaying the footer
 *
 */

?>
		<footer class="footer bg-black small text-center text-white-50">
            <div class="container">Copyright © Naza Alakija 2020</div>
        </footer>

        <div class="modal fade left contentmodal humanitarian" id="contentModal" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="container-fluid modal-content">
                    <div class="modal-header d-flex justify-content-between align-items-center fixed-top">
                        <div class="row w-100 h-100">
                            <div class="col-sm-4 col-8 d-flex align-items-center">
                                <h4 class="modal-page-title animate__animated">Humanitarian</h4>
                            </div>
                            <div class="col-sm-4 text-center d-sm-block d-none">
                                <a class="naza-brand animate__animated" href="#welcome">Naza Alakija</a>
                            </div>
                            <div class="col-sm-4 col-4">
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid modal-body p-0">
                        <div class="row w-100 no-gutter align-items-stretch">
                            <div class="col-lg-6">
                                <img class="img-fluid" src="<?php the_field('humanitarian_modal_image_1'); ?>" alt="" />
                            </div>
                            <div class="col-lg-6">
                                <div class="row h-100 d-flex justify-content-start align-items-center"">
                                    <div class="col-xl-9 col-lg-12 p-5">
                                        <h3 class="mb-4">Women are the pillars of our community</h3>
                                        <p>Empowerment and equality of women is imperative as women’s rights are fundamentally human rights. Without equality, development is hindered, and sustainable economic growth is disrupted. The climate crisis has and will continue to exacerbate inequality prevalent in different parts of the world.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutter align-items-stretch">
                            <div class="col-lg-6 order-lg-1 order-md-2 order-sm-2 order-2">
                                <div class="row h-100 d-flex justify-content-end align-items-center">
                                    <div class="col-xl-9 col-lg-12 p-5">
                                        <h3 class="mb-4">Children are the future; their rights are inextricably linked</h3>
                                        <p>Educating those in vulnerable communities and ensuring resilience, mitigation and adaptation are part of that education - in the classroom and beyond.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 order-lg-2 order-md-1 order-sm-1 order-1">
                                <img class="img-fluid" src="<?php the_field('humanitarian_modal_image_2'); ?>" alt="" />
                            </div>
                        </div>
                        <div class="row no-gutter align-items-stretch">
                            <div class="col-lg-6">
                                <img class="img-fluid" src="<?php the_field('humanitarian_modal_image_3'); ?>" alt="" />
                            </div>
                            <div class="col-lg-6">
                                <div class="row h-100 d-flex justify-content-start align-items-center">
                                    <div class="col-xl-9 col-lg-12 p-5">
                                        <h3 class="mb-4">Climate change threatens development progress for women and children</h3>
                                        <p>Climate change disproportionately affects women and children as they have less access to basic human rights and are vulnerable to exploitation and systematic violence. During periods of chaos, these issues escalate and are exacerbated.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutter align-items-stretch">
                            <div class="col-lg-6 order-lg-1 order-md-2 order-sm-2 order-2">
                                <div class="row h-100 d-flex justify-content-end align-items-center">
                                    <div class="col-xl-9 col-lg-12 p-5">
                                        <h3 class="mb-4">Raising awareness about climate change, it’s impacts and adaptation / mitigation techniques by educating vulnerable populations</h3>
                                        <p>It's near, it's here, so we adapt. It is essential that we educate the most vulnerable communities on adaptation, resilience, mitigation. The broad public require education on its impact, how it will affect their livelihood, and how they can acclimate accordingly.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 order-lg-2 order-md-1 order-sm-1 order-1">
                                <img class="img-fluid" src="<?php the_field('humanitarian_modal_image_4'); ?>" alt="" />
                            </div>
                        </div>
                        <div class="row no-gutter align-items-stretch">
                            <div class="col-lg-6">
                                <img class="img-fluid" src="<?php the_field('humanitarian_modal_image_5'); ?>" alt="" />
                            </div>
                            <div class="col-lg-6">
                                <div class="row h-100 d-flex justify-content-start align-items-center">
                                    <div class="col-xl-9 col-lg-12 p-5">
                                        <h3 class="mb-4">Addressing the social dimension of climate change</h3>
                                        <p>Addressing the social dimension of climate change is critical to prevent unrest. We must collectively ensure that the policies and measures under the Nationally Determined Contributions (NDCs) design a just transition for those whose jobs and livelihoods will be impacted by the transition towards a carbon neutral world.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row no-gutter align-items-stretch">
                            <div class="col-lg-6 order-lg-1 order-md-2 order-sm-2 order-2">
                                <div class="row h-100 d-flex justify-content-end align-items-center">
                                    <div class="col-xl-9 col-lg-12 p-5">
                                        <h3 class="mb-4">Empowering Middle Eastern and African SMEs to develop innovative climate solutions</h3>
                                        <p>As we witnessed with the COVID19 pandemic, problems of one country affects us all. The Middle East and Africa are the two areas which I focus on in empowering SME’s to develop innovative solutions against the impacts of climate change. Doing this will allow us to accelerate the transition to a 100% renewable energy</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 order-lg-2 order-md-1 order-sm-1 order-1">
                                <img class="img-fluid" src="<?php the_field('humanitarian_modal_image_6'); ?>" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--About Naza Modal-->
        <div class="modal fade left contentmodal aboutnaza" id="aboutNaza" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
              <div class="container-fluid modal-content">
                <div class="modal-header d-flex justify-content-between align-items-center fixed-top">
                    <div class="row w-100 h-100">
                        <div class="col-sm-4 col-8 d-flex align-items-center">
                            <h4 class="modal-page-title animate__animated">About Naza</h4>
                        </div>
                        <div class="col-sm-4 text-center d-sm-block d-none">
                            <a class="naza-brand animate__animated" href="#welcome">Naza Alakija</a>
                        </div>
                        <div class="col-sm-4 col-4">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="container modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php if ( function_exists( 'wpsp_display' ) ) wpsp_display( 72 ); ?>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <!--About Naza Modal-->

        <!--Environmentalist Modal-->
        <div class="modal fade left contentmodal environmentalist" id="envModal" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
              <div class="container-fluid modal-content">
                <div class="modal-header d-flex justify-content-between align-items-center fixed-top">
                    <div class="row w-100 h-100">
                        <div class="col-sm-4 col-8 d-flex align-items-center">
                            <h4 class="modal-page-title animate__animated">Environmentalist</h4>
                        </div>
                        <div class="col-sm-4 text-center d-sm-block d-none">
                            <a class="naza-brand animate__animated" href="#welcome">Naza Alakija</a>
                        </div>
                        <div class="col-sm-4 col-4">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="container-fluid modal-body p-0">
                    <div class="row no-gutter align-items-stretch">
                        <div class="col-lg-6">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/env-modal-1.jpeg" alt="" />
                        </div>
                        <div class="col-lg-6">
                            <div class="row h-100 d-flex justify-content-left align-items-center">
                                <div class="col-lg-9 p-5">
                                    <h3 class="mb-4">The climate crisis endangers progress on many of the necessary elements for human development codified in the SDGs.</h3>
                                    <p>
                                        From rising temperatures to rising sea levels and drought: anywhere in the world you look today, you can see the devastating impacts of climate change. While living in Nigeria, I experienced the longer rainy seasons which have had a direct impact on crop yields. Lower yields mean less income for families and that often means children are forced into early marriages and to trade school work for farm work in order to supplement family income; young girls and women are especially vulnerable to being pushed into transactional sex.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutter align-items-stretch">
                        <div class="col-lg-6 order-lg-1 order-md-2 order-sm-2 order-2">
                            <div class="row h-100 d-flex justify-content-end align-items-center">
                                <div class="col-lg-9 p-5">
                                    <p>
                                        As I researched ways to mitigate the effects I was observing and discussed with those around me, it became clear that there is a knowledge gap even among those most directly impacted by the changing climate. So, I decided to take action: 
                                        <ul>
                                            <li>I founded S.A.G.E. Innovation, an ecosystem of experts and entrepreneurs committed to make a difference. </li>
                                            <li>I started working with international agencies such as UNICEF supporting different projects and initiatives. </li>
                                            <li>I supported the prosthetics for child victims of bomb blasts in war zones through the UN’s WASH section with the help of funding.</li>
                                        </ul>
                                    </p>
                                    <h3 class="mb-4">Part of that action is raising awareness by working with policymakers, vulnerable populations and the global community.</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 order-lg-2 order-md-1 order-sm-1 order-1">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/env-modal-2.jpeg" alt="" />
                        </div>
                    </div>
                    <div class="row no-gutter align-items-stretch">
                        <div class="col-lg-6">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/env-modal-3.jpeg" alt="" />
                        </div>
                        <div class="col-lg-6">
                            <div class="row h-100 d-flex justify-content-left align-items-center">
                                <div class="col-lg-9 p-5">
                                    <h3 class="mb-4">In the short term, I am addressing the social dimension of climate change by:</h3>
                                    <p>
                                        <ul>
                                            <li>Ensuring policies address and protect against it.</li>
                                            <li>Building climate resilience in low income communities and supporting people already affected by its impact.</li>
                                            <li>Empowering Middle Eastern and African SMEs to develop innovative climate solutions.</li>
                                        </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row my-5">
                        <div class="container">
                            <div class="col-lg-12">
                                <h3 class="mb-5">We’re on the front line of climate change, so let’s protect the most vulnerable global citizens and save our planet. Join me!</h3>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <!--Environmentalist Modal-->

        <!--Philantropist Modal-->
        <div class="modal fade left contentmodal philanthropist" id="philModal" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
              <div class="container-fluid modal-content">
                <div class="modal-header d-flex justify-content-between align-items-center fixed-top">
                    <div class="row w-100 h-100">
                        <div class="col-sm-4 col-8 d-flex align-items-center">
                            <h4 class="modal-page-title animate__animated">Philanthropist</h4>
                        </div>
                        <div class="col-sm-4 text-center d-sm-block d-none">
                            <a class="naza-brand animate__animated" href="#welcome">Naza Alakija</a>
                        </div>
                        <div class="col-sm-4 col-4">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="container modal-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <h3 class="mb-4">GIGA</h3>
                            <p>
                                Recognizing that internet access is now a fundamental necessity for children around the world to learn, GIGA is an initiative designed to connect every young person in the world to the Internet. Launched in September 2019, this initiative by UNICEF and ITU seeks to map all schools in the world, categorize them by internet connectivity and deploy funding to create/increase internet access, thereby bridging the digital divide. GIGA is a long term effort, but there has been a special Covid-19 rapid response.
                            </p>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h3 class="mb-4">Plasticos Ventures: Plastic Brick Schools</h3>
                            <p>
                                Plasticos Ventures is using recycled plastic waste to build safe and affordable schools in partner countries. In 2018, UNICEF and Conceptos Plasticos launched a partnership to build schools in Cote d’Ivoire from 100% recycled plastic bricks. The success of those schoosl encouraged the government to sign on to scale construction nationally, leading to construction of the first recycled plastic brick factory in Africa. By formalizing a women-led plastic recycling economy, UNICEF and Conceptos Plasticos design a model to empower mothers, increasing the likelihood their children go to school.
                            </p>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h3 class="mb-4">Nanosatellites for Girls</h3>
                            <p>
                                UNICEF is encouraging girls to get into STEM and to develop the knowledge and competencies of girls in the development of nanosatellites. 20 girls aged 14 to 35 years from different regions of Kazakhstan will undertake courses on the creation of a nanosatellite over five months. By the end of the project, the participants will create models of nanosatellites that will be launched into the upper atmosphere and the girls will collect data. Upon launch of the project  a female astronaut will become an advisor and launch a campaign around women, girls, space and STEM education
                            </p>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h3 class="mb-4">Safe Return: Yerwa Girls School</h3>
                            <p>
                                SAGE made a donation to ensure the safe return to school of 5,600 girls in Borno State, after extended school closures due to the coronavirus pandemic. Specifically, the donation is being used to ensure the girls have a safe, well-equipped, corona-rady school to return to and that their community is also equipped to face down COVID. The donation covers: risk communication & community engagement, re-opening schools, providing equipment for the health center, essential supplies for the COVID response including PPEs and repairing boreholes to ensure availability of touchless handwashing facilities
                            </p>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h3 class="mb-4">UNRWA: Gaza Back to School Post COVID</h3>
                            <p>
                                UNRWA's mission is to help Palestine refugees in Jordan, Lebanon, Syria, West Bank and Gaza achieve their full potential,  pending a just solution to their plight. The COVID-19 pandemic has only exasperated the widespread insecurity that was originally the result of the blockade and the man-made humanitarian crisis in Gaza. SAGE's donation will go to their Back to School Program as well as to elevating the profile of the work the agency does to assist it in attracting donors.
                            </p>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <!--Philantropist Modal-->


        <!--Features Modal-->
        <div class="modal fade left contentmodal features" id="features" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
              <div class="container-fluid modal-content">
                <div class="modal-header d-flex justify-content-between align-items-center fixed-top">
                    <div class="row w-100 h-100">
                        <div class="col-sm-4 col-8 d-flex align-items-center">
                            <h4 class="modal-page-title animate__animated">Features</h4>
                        </div>
                        <div class="col-sm-4 text-center d-sm-block d-none">
                            <a class="naza-brand animate__animated" href="#welcome">Naza Alakija</a>
                        </div>
                        <div class="col-sm-4 col-4">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="container modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php if ( function_exists( 'wpsp_display' ) ) wpsp_display( 68 ); ?>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <!--Features Modal-->

        <!--Reads Modal-->
        <div class="modal fade left contentmodal reads" id="reads" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
              <div class="container-fluid modal-content">
                <div class="modal-header d-flex justify-content-between align-items-center fixed-top">
                    <div class="row w-100 h-100">
                        <div class="col-sm-4 col-8 d-flex align-items-center">
                            <h4 class="modal-page-title animate__animated">Reads</h4>
                        </div>
                        <div class="col-sm-4 text-center d-sm-block d-none">
                            <a class="naza-brand animate__animated" href="#welcome">Naza Alakija</a>
                        </div>
                        <div class="col-sm-4 col-4">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="container modal-header-image d-flex justify-content-center align-items-end">
                <div class="row h-100">
                        <div class="col-sm-12">
                            <h3 class="text-center">Books that inspired me personally and professionally</h3>
                        </div>
                    </div>
                </div>
                <div class="container modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="readsAccordion" class="reads-accordion">
                                <div class="card">
                                    <div class="card-header" id="twentyTwenty">
                                        <h2 class="mb-0">
                                            <button class="d-flex align-items-center justify-content-between btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            2020
                                            <span class="fa-stack fa-sm">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-minus fa-stack-1x fa-inverse"></i>
                                            </span>
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseOne" class="collapse show" aria-labelledby="twentyTwenty" data-parent="#readsAccordion">
                                        <div class="card-body">
                                            <?php if ( function_exists( 'wpsp_display' ) ) wpsp_display( 76 ); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="twentyNineteen">
                                        <h2 class="mb-0">
                                            <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2019
                                            <span class="fa-stack fa-2x">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                            </span>
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="twentyNineteen" data-parent="#readsAccordion">
                                        <div class="card-body">
                                            <?php if ( function_exists( 'wpsp_display' ) ) wpsp_display( 79 ); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h2 class="mb-0">
                                            <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            2018
                                            <span class="fa-stack fa-2x">
                                                <i class="fas fa-circle fa-stack-2x"></i>
                                                <i class="fas fa-plus fa-stack-1x fa-inverse"></i>
                                            </span>
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#readsAccordion">
                                        <div class="card-body">
                                            <?php if ( function_exists( 'wpsp_display' ) ) wpsp_display( 80 ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <!--Reads Modal-->

        <!--Mobile Contact Modal-->
        <div class="modal fade left contentmodal contactnaza" id="contactNaza" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-header d-flex justify-content-between align-items-center fixed-top">
                        <div class="row w-100 h-100">
                            <div class="col-sm-4 col-8 d-flex align-items-center">
                                <h4 class="modal-page-title animate__animated">Contact me</h4>
                            </div>
                            <div class="col-sm-4 text-center d-sm-block d-none">
                                <a class="naza-brand animate__animated" href="#welcome">Naza Alakija</a>
                            </div>
                            <div class="col-sm-4 col-4">
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div id="contactModalForm">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Mobile Contact Modal-->

		<?php wp_footer(); ?>
		<script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/vendor/fullpage/js/fullpage.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/vendor/jquery-easing/jquery.easing.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tilt.js/1.2.1/tilt.jquery.min.js"></script>
        <!-- Core theme JS-->
        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/scripts.min.js"></script>

	</body>
</html>
