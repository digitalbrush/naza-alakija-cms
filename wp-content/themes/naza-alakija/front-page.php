<?php
/**
 * The custom fullpage template file
 * 
 * Template Name: Custom Homepage
 * Template Post Type: page
 */

get_header();
?>
        <div id="fullpage">
            <header class="section masthead">
                <div class="container-fluid">
                    <div class="hero-bg-text animate__animated animate__fadeIn animate__slower">
                        <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/hero-naza-letters.svg" alt=""/>
                    </div>
                    <div class="hero-image">
                        <img class="img-fluid hero-naza animate__animated" src="<?php echo get_template_directory_uri(); ?>/assets/img/naza-portrait-web.png" alt="" />
                    </div>
                   
                    <div class='icon-scroll'></div>
                </div>
            </header>
            <!-- About-->
            <section class="section about-section">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-4 col-sm-4 col-6 about-block humanitarian animate__animated">
                            <a href="#humanitarian">
                                <h1 class="block-h1">01</h1>
                                <div class="about-image">
                                    <div class="mask">
                                        <button class="btn btn-readmore">Read More</button>
                                    </div>
                                </div>
                                <h3 class="about-title">
                                    <span class="about-dot"></span>Humanitarian
                                </h3>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-6 about-block environmentalist animate__animated">
                            <a href="#environmentalist">
                                <h1 class="block-h1">02</h1>
                                <div class="about-image">
                                    <div class="mask">
                                        <button class="btn btn-readmore">Read More</button>
                                    </div>
                                </div>
                                <h3 class="about-title">
                                    <span class="about-dot"></span>Environmentalist
                                </h3>
                            </a>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-6 about-block philanthropist animate__animated">
                            <a href="#philanthropist">
                                <h1 class="block-h1">03</h1>
                                <div class="about-image">
                                    <div class="mask">
                                        <button class="btn btn-readmore">Read More</button>
                                    </div>
                                </div>
                                <h3 class="about-title">
                                    <span class="about-dot"></span>Philanthropist
                                </h3>
                            </a>
                        </div>
                        <div class="col-xl-8 col-lg-10 col-sm-12 col-6">
                            <h1 class="about-heading-text"><?php the_field('about_vision_statement'); ?></h1>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Projects-->
            <section class="section humanitarian-section quarter-circle-bottom-left">
                <div class="container pt-60">
                    <div class="h-image">
                        <img load="lazy" class="img-fluid h-naza" src="<?php echo get_template_directory_uri(); ?>/assets/img/naza-water-pump.png" alt="" />
                    </div>
                    <div class="row d-flex justify-content-end mb-2">
                        <div class="col-lg-9 col-sm-12">
                            <div class="row justify-content-start small-gutters">
                                <div class="col-sm-4 col-6 humanitarian-block">
                                    <img class="img-fluid" src="<?php the_field('humanitarian_image_1'); ?>" alt="" />
                                </div>
                                <div class="col-sm-4 col-6 humanitarian-block">
                                    <div class="hblock-text w-100 h-100 my-auto">
                                        <h5 class="animate__animated"><span>Women</span> are the pillars of our community</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row d-flex justify-content-end mb-2">
                        <div class="col-lg-9 col-sm-12">
                            <div class="row justify-content-end small-gutters">
                                <div class="col-sm-4 col-6 humanitarian-block">
                                    <img class="img-fluid" src="<?php the_field('humanitarian_image_2'); ?>" alt="" />
                                </div>
                                <div class="col-sm-4 col-6 humanitarian-block">
                                    <img class="img-fluid" src="<?php the_field('humanitarian_image_3'); ?>" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row d-flex justify-content-end mb-2">
                        <div class="col-lg-9 col-sm-12">
                            <div class="row justify-content-center small-gutters">
                                <div class="col-sm-4 col-6 humanitarian-block">
                                    <div class="hblock-text bg-brown w-100 h-100">
                                        <h5 class="animate__animated"><span>Children</span> are the future</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-6 humanitarian-block">
                                    <img class="img-fluid" src="<?php the_field('humanitarian_image_4'); ?>" alt="" />
                                </div>
                                <div class="col-sm-4 col-6 humanitarian-block d-sm-block d-none">
                                    <img class="img-fluid" src="<?php the_field('humanitarian_image_5'); ?>" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row d-flex justify-content-end align-items-center mb-2">
                        <div class="col-lg-9 col-sm-12">
                            <div class="row justify-content-end small-gutters">
                                <div class="col-sm-4 col-6 humanitarian-block">
                                    <img class="img-fluid" src="<?php the_field('humanitarian_image_6'); ?>" alt="" />
                                </div>
                                <div class="col-sm-4 col-6 humanitarian-block">
                                    <div class="hblock-text d-block bg-transparent w-100 h-100">
                                        <h5>...their rights are inextricably linked</h5>
                                        <a class="btn btn-readmore" data-toggle="modal" data-target="#contentModal">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h1 class="side-page-title">Humanitarian</h1>
            </section>
            <!-- Signup-->
            <section class="section environment-section">
                <div class="container-fluid p-0">
                    <div class="row d-flex justify-content-start align-items-sm-stretch align-items-end no-gutter">
                        <div class="col-lg-6 col-sm-6 col-3 env-bg">
                            <h1 class="env-heading-text animate__animated"><?php the_field('environmentalist_statement'); ?></h1>
                        </div>
                        <h1 class="side-page-title">Environmentalist</h1>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-9 env-body">
                            <div class="row no-gutters">
                                <div class="col-6">
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/naza-plastic-building.jpg" alt="" />
                                </div>
                                <div class="col-6 d-sm-block d-none">
                                </div>
                            </div>
                            <div class="row no-gutters">
                                <div class="col-6 d-flex justify-content-center align-items-center">
                                    <a class="btn btn-readmore" data-toggle="modal" data-target="#envModal">Read More</a>
                                </div>
                                <div class="col-6">
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/naza-planting-tree.jpg" alt="" />
                                </div>
                            </div>
                            <div class="row no-gutters">
                                <div class="col-6">
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/naza-talking-to-class.jpg" alt="" />
                                </div>
                                <div class="col-sm-6 d-sm-block d-none">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Signup-->
            <section class="section philanthropy-section">
                <div class="container-fluid pt-60">
                    <div class="row">
                        <div class="col-md-8 col-sm-10">
                            <h2 class="phil-heading-text animate__animated"><?php the_field('philanthropist_statement'); ?></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row no-gutter">
                                <div class="col-5">
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/lady-in-art-gallery.jpg" alt="" />
                                </div>
                                <div class="col-7 sage-logo animate__animated">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 301 385" enable-background="new 0 0 301 385" xml:space="preserve">
                                        <g id="logoSage">
                                            <path d="M0.5,236.3c0.2-2,0.4-4,0.7-5.9c2-14.3,8.4-27.6,18.4-38c11.1-12,26.3-19.7,42.6-21.4
                                                c16.6-2,31.9,1.9,46,10.6c3.1,2,6.2,4.2,9.1,6.5c2.4,1.8,2.4,4.2,0.4,6.3c-3.7,3.9-7.5,7.8-11.2,11.7c-2.1,2.2-3.6,2.2-5.9,0.2
                                                c-9-7.7-19.3-11.5-31.1-10.9C57.2,196,47,201.3,39,210.6c-15.2,17.5-14.7,43.7,1.2,60.6c9.5,10.1,21.3,14.5,35.1,13.7
                                                c6.5-0.5,12.8-2,18.8-4.4c0.4-0.2,0.8-0.3,0.8-0.9c0-5.1,0-10.3,0-15.4c0-0.2,0-0.3-0.1-0.5h-1.1c-4.7,0-9.4,0-14.1,0
                                                c-2.6,0-4-1.4-4-4c0-4.8,0-9.7,0-14.5c0-2.7,1.4-4.1,4-4.1c12.2,0,24.4,0,36.6,0c2-0.2,3.8,1.3,3.9,3.3c0,0.1,0,0.3,0,0.4
                                                c0,0.4,0,0.7,0,1.1c0,15.8,0,31.7,0,47.5c0.2,2.2-1.1,4.3-3.1,5.3c-10,5.3-20.8,8.9-32,10.6c-7.9,1.4-15.9,1.6-23.8,0.6
                                                c-23.4-3.4-41-15.4-52.6-36.1c-4.7-8.6-7.4-18.1-8-27.8c0-0.3-0.1-0.6-0.2-0.9L0.5,236.3L0.5,236.3z M240.9,384.5
                                                c2.3-0.8,3.1-2.5,3-4.9c-0.1-4.6,0-9.3,0-13.9c0-3.3-1.3-4.6-4.6-4.6h-56.7v-34h1.3c15.4,0,30.7,0,46.1,0c2.8,0,4.3-1.4,4.3-4.2
                                                c0-5,0-10,0-15c0-2.9-1.5-4.4-4.4-4.4h-47.3v-31.9h57c2.9,0,4.3-1.4,4.3-4.3c0-4.9,0-9.7,0-14.6c0-3.1-1.4-4.5-4.4-4.5
                                                c-25.9,0-51.9,0-77.8,0c-2.3-0.2-4.2,1.5-4.4,3.7c0,0.2,0,0.5,0,0.7c0.1,42.4,0.1,84.8,0,127.2c0,2.4,0.8,4,3.1,4.7H240.9z
                                                    M99.5,0.5c-0.4,0.1-0.7,0.1-1.1,0.2C93.6,1,88.8,1.9,84.2,3.5C63.3,10.8,55,31.5,61,49.9c2.5,7.7,7.5,13.7,13.8,18.6
                                                c6.2,4.7,13,8.4,20.2,11.2c4.7,1.8,9.2,4,13.6,6.6c3.2,1.8,6,4.2,8.3,7.1c3,3.9,4.3,8.2,3.1,13c-1.4,5.8-5.4,9.1-10.8,10.9
                                                c-5.3,1.8-10.6,1-15.8-0.7c-7.2-2.3-13.7-6.1-19.9-10.3c-2.6-1.8-5.4-1.2-7,1.6c-2.2,3.8-4.5,7.7-6.7,11.5
                                                c-0.3,0.5-0.5,0.9-0.7,1.4c-0.7,1.3-0.4,2.8,0.6,3.8c1.3,1.3,2.6,2.5,4,3.6c15,10.9,31.7,14.4,49.8,11.6
                                                c9.4-1.4,18.1-5.9,24.6-12.9c11.5-12.1,13.3-30.6,4.3-44.7c-3.8-5.8-8.9-10.2-14.6-14c-6.2-3.9-12.7-7.1-19.5-9.7
                                                c-4.6-1.8-9.1-4-13.4-6.5c-3.1-1.8-5.7-4.2-7.8-7.1c-4.6-6.7-3.1-14.7,3.8-19.2c2.5-1.6,5.3-2.5,8.2-2.8c4.5-0.5,8.8,0.6,13,2.1
                                                c5.7,2.2,11.1,4.9,16.2,8.3c1.5,1.1,3.6,1.1,5-0.1c0.8-0.5,1.4-1.1,1.9-1.8c2.6-3.8,5.2-7.6,7.7-11.5c1.3-2,0.8-4.7-1.1-6.2
                                                C133.2,7.3,123,3.1,112.3,1.3c-2.1-0.3-4.3-0.5-6.4-0.8L99.5,0.5z M300.5,255.3c-0.1-0.4-0.3-0.9-0.4-1.3
                                                c-1.5-5.8-6.8-9.6-12.8-9.2c-6,0.4-10.8,5.1-11.4,11.1c-0.6,5.9,3.1,11.3,8.8,13c5.7,1.7,11.8-1,14.4-6.3c0.5-1.3,1-2.5,1.3-3.9
                                                v-3.4H300.5z M273,211.3c0.1,0,0.1,0,0.2,0c2.9-0.1,4.4-2.4,3.3-5.1c-0.1-0.2-0.2-0.4-0.3-0.6l-50-109.5
                                                c-3.1-6.9-6.3-13.8-9.4-20.6c-0.5-1.6-2.1-2.6-3.7-2.4c-0.4,0-0.7,0-1.1,0c-2-0.2-3.3,0.8-4.1,2.6c-20,43.4-40,86.7-60.1,130.1
                                                c-1.4,3.1,0.1,5.5,3.5,5.5c5.4,0,10.8,0,16.2,0c2.7,0.1,5.1-1.4,6-3.9c3-6.7,6.1-13.4,9.1-20.1c0.2-0.6,0.8-1,1.4-0.9
                                                c18.7,0,37.5,0,56.2,0c0.6-0.1,1.2,0.3,1.3,0.9c1.4,3.3,2.9,6.5,4.4,9.7c1.6,3.4,3.1,6.8,4.7,10.3c1.2,2.8,3.2,4.2,6.4,4.1
                                                c2.5-0.1,5.1,0,7.6,0L273,211.3z M192.6,164.7c0.2-0.4,0.3-0.7,0.4-1c6-13.6,12.1-27.1,18.1-40.7c0.1-0.2,0.2-0.4,0.3-0.6
                                                c0.3-0.5,0.6-0.5,0.9,0c0.1,0.2,0.2,0.4,0.3,0.5c6.2,13.6,12.4,27.2,18.6,40.9c0.1,0.3,0.2,0.6,0.4,1h-39V164.7z M19.3,151
                                                c6.8,0.1,12.3-5.4,12.4-12.2l0,0c0-6.8-5.4-12.3-12.2-12.4s-12.3,5.4-12.4,12.2v0.1C7,145.5,12.5,151,19.3,151z M189.9,45.3
                                                c0-6.8-5.5-12.3-12.2-12.3c-6.8,0-12.3,5.4-12.4,12.2c-0.1,6.7,5.3,12.3,12,12.4c0.1,0,0.1,0,0.2,0
                                                C184.2,57.6,189.8,52.2,189.9,45.3C189.9,45.4,189.9,45.3,189.9,45.3z M109.1,338.2c-6.8,0-12.3,5.4-12.3,12.2v0.1
                                                c0,6.8,5.4,12.2,12.2,12.3c0,0,0,0,0.1,0c6.8,0.1,12.3-5.4,12.4-12.2v-0.1C121.4,343.7,115.9,338.2,109.1,338.2z"/>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row no-gutter">
                                <div class="col-5 d-flex justify-content-start align-items-center">
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/lady-using-sanitizer.jpg" alt="" />
                                </div>
                            </div>
                            <div class="row no-gutter">
                                <div class="col-6 d-flex justify-content-center align-items-center">
                                    <a class="btn btn-readmore" data-toggle="modal" data-target="#philModal">Explore</a>
                                </div>
                                <div class="col-5 px-3">
                                    <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/girl-holding-soap.jpg" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h1 class="side-page-title">Philanthropist</h1>
            </section>
            <!-- Contact-->
            <section class="section contact-section">
                <div class="container-fluid p-0">
                    <div class="row d-flex justify-content-center">
                        <div class="col-sm-10">
                            <h2 class="contact-heading-text animate__animated"><?php the_field('contact_naza_statement'); ?></h2>
                        </div>
                    </div>
                    <div class="row d-flex justify-content-center no-gutter">
                        <div class="col-sm-4 col-7">
                            <img class="naza-contact animate__animated img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/naza-contact-image.jpg" alt="" />
                            <div class="social d-flex justify-content-center">
                                <div class="social-icons">
                                    <a class="mx-2" href="https://www.instagram.com/nazaalakija" target="_blank">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                    <a class="mx-2" href="https://twitter.com/nazaalakija" target="_blank">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 col-5 mb-3 mb-md-0">
                            <div class="row d-flex h-100 justify-content-center align-items-center">
                                <div class="col-md-8 col-sm-9 px-4">
                                    <div class="contact-form" id="contactForm">
                                        <?php echo do_shortcode( '[ninja_form id=1]' ); ?>
                                    </div>
                                    <div class="d-flex d-sm-none justify-content-center align-items-center">
                                        <a class="btn btn-readmore" data-toggle="modal" data-target="#contactNaza">Contact Me</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>



        <?php
        get_footer();