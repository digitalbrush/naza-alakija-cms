<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

	<meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Naza Alakija - Humanitarian. Environmentalist. Philanthropist.</title>
        <link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://kit.fontawesome.com/6306918092.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/vendor/fullpage/css/fullpage.min.css" />
        

		<?php wp_head(); ?>

	</head>

    <body <?php body_class(); ?>>

    <!-- <div id="overlayer"></div>
    <span class="loader">
    <span class="loader-inner"></span>
    </span> -->

		<nav class="navbar fixed-top" id="mainNav">
            <div class="container-fluid d-flex justify-content-center align-items-center">
                <div class="nav-button col-sm-4 col-3 d-flex align-items-center no-gutter">
                    <button class="navbar-toggler navbar-toggler-left" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <a href="#" class="nav-menu-icon">
                            <div class="line"></div>
                            <div class="line"></div>
                            <div class="line"></div>
                        </a>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
						<?php
							wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'navbar-nav',
							) );
						?>
                    </div>
                </div>
                <div class="logo col-sm-4 col-6 text-md-center animate__animated">
                    <a class="navbar-brand" href="<?php echo home_url(); ?>">Naza Alakija</a>
                </div>
                <ul class="top-nav animate__animated col-sm-4 col-3 text-right">
                    <li>
                        <a class="mx-2" href="#contact"><i class="fa fa-comment-o"></i></a>
                      <!-- <a href="#">
                        About me -->
                      </a>
                    </li>
                </ul>
            </div>
        </nav>
